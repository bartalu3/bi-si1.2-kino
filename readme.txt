XXXTremeCinema

Požadavky:
1. Python 3.6 nebo novější
2. Django 3.2
3. cx-Oracle==8.1.0
4. Oracle DB client - https://www.oracle.com/database/technologies/instant-client/winx64-64-downloads.html

Instalace databáze:
1. Vytvoření databáze a spuštění SQL skriptu `create.sql`.
2. Konfigurace databáze je v souboru `cinema/settings.py`, řádky 79 - 88:
	2.1: ENGINE: Ve výchozím stavu je použita Oracle databáze.
	2.2: USER: Uživatelské jméno pro přihlášení k databázi.
	2.3: NAME: Název databáze.
	2.4: PASSWORD: Heslo pro přihlášení uživatele.
	2.5: HOST: Adresa, na které se nachází databázový server.
	2.6: PORT: Port databázové služby.

Spuštění:
1. Spuštění serveru ./manage.py runserver
2. Web je nyní dostupný na adrese http://127.0.0.1:8000

Ovládání aplikace:
1. Pro správu záznamů je třeba se přihlásit - v pravém horním rohu tlačítko login. Přihlašovací údaje pro správce jsou:
	email: zralok@fit.cvut.cz
	heslo: zralok

2. Alternativně je možné vytvořit nového uživatele příkazem ./manage.py createsuperuser
3. Správa filmů a licencí je možná pomocí sidebaru vlevo.

Poznámky:
1. Přihlašování uživatelů je pro tuto fázi projektu zprovozěno jen pro vývojové účely - Přihlášení pouze pro administrátory.
2. Aplikace využívá školní datázový Oracle server a není tedy potřeba server řešit lokálně.


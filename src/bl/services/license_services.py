from src.dl.data_entities.license import License
from cinema.data_managers_factory import DataManagerFactory

## class LicenseService to manage licenses in bussines layer
class LicenseServices:
    ## constructor
    def __init__(self):
        self.license_manager = DataManagerFactory.Licenses

    ## get_all gets licenses from license manager
    # \param self
    # \returns array of all license entities
    def get_all(self):
        return self.license_manager.get_all()

    ## store licence via license manager
    # \param self
    # \param licence is licence to be added
    def add(self, licence: License):
        self.license_manager.add(licence)

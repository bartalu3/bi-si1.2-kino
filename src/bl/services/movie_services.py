from src.dl.data_entities.movie import Movie
from cinema.data_managers_factory import DataManagerFactory

## class MovieService to manage movies in bussines layer
class MovieServices:
    ##cosntructor
    def __init__(self):
        self.movie_manager = DataManagerFactory.Movies

    ## get_titles gets titles from movie manager
    # \param self
    # \return array of movie titles with ids
    def get_titles(self):
        return self.movie_manager.get_titles()

    ## get_all gets all movies from movie manager
    # \param self
    # \return array of all movie entities
    def get_all(self):
        return self.movie_manager.get_all()

    ## get_all stores movie via movie manager
    # \param self
    # \param movie is movie to be stored
    def add(self, movie: Movie):
        self.movie_manager.add(movie)

## data entity representing movie
class Movie:
    ## constructor
    # \param mv_title is title of movie
    # \param mv_price is price of movie
    # \param mv_id is id of movie
    def __init__(self, mv_title: str, mv_price: int, mv_id: int = None):
        self.movie_id = mv_id
        self.title = mv_title
        self.price_of_movie = mv_price

    price_of_movie: int
    title: str
    movie_id: int

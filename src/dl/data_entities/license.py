from datetime import date
from src.dl.data_entities.movie import Movie
from src.dl.data_entities.planner import Planner

## data entity prepresenting license
class License:
    ## constructor
    # \param self
    # \param ls_from from this date is license valid
    # \param ls_to to this date is license valid
    # \param ls_price is price of license
    # \param ls_movie license to this movie
    # \param ls_id id of the license
    # \param ls_planner planner who acquired this license
    def __init__(self, ls_from: date, ls_to: date, ls_price: int, ls_movie: Movie, ls_id: int = None, ls_planner=None):
        self.license_id = ls_id
        self.price = ls_price
        self.from_field = ls_from
        self.to = ls_to
        self.movie = ls_movie
        self.planner = ls_planner

    license_id: int
    price: int
    from_field: date
    to: date
    movie: Movie
    planner: Planner

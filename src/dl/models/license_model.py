from django.db import models

from src.dl.models.planner_model import PlannerModel
from src.dl.models.movie_model import MovieModel

## License is model representation of movie license ...
# attributes: \n
#   price is the price of movie \n
#   from_field is the start date of license \n
#   to is the end date of license \n
#   license_id is the id of license \n
#   movie is the movie that has this license \n
#   planner is the planner that has wrote down this license \n
class LicenseModel(models.Model):
    price = models.BigIntegerField(unique=False)
    from_field = models.DateField(db_column='from',
                                  unique=False)  # Field renamed because it was a Python reserved word. This field type is a guess.
    to = models.DateField(unique=False)  # This field type is a guess.
    license_id = models.BigIntegerField(primary_key=True)
    movie = models.ForeignKey(MovieModel, models.DO_NOTHING, unique=False)
    planner = models.ForeignKey(PlannerModel, models.DO_NOTHING, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'license'

from django.db import models


class CleanerModel(models.Model):
    cleaner_id = models.IntegerField(primary_key=True)

    class Meta:
        managed = False
        db_table = 'cleaner'

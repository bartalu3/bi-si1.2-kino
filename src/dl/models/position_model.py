from django.db import models


class PositionModel(models.Model):
    from_field = models.TextField(db_column='from',
                                  unique=False)  # Field renamed because it was a Python reserved word. This field type is a guess.
    to = models.TextField(blank=True, null=True)  # This field type is a guess.
    salary_class = models.TextField(unique=False)  # This field type is a guess.
    position_id = models.BigIntegerField(primary_key=True)
    usher = models.BooleanField(unique=False)
    cleaner = models.BooleanField(unique=False)
    receptionist = models.BooleanField(unique=False)

    class Meta:
        managed = False
        db_table = 'position'
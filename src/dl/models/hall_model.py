from django.db import models


class HallModel(models.Model):
    capacity = models.BigIntegerField(unique=False)
    title = models.TextField(unique=False)  # This field type is a guess.
    nuber_of_rows = models.BigIntegerField(unique=False)
    size_of_screen = models.BigIntegerField(unique=False)
    hall_id = models.BigIntegerField(primary_key=True)

    class Meta:
        managed = False
        db_table = 'hall'
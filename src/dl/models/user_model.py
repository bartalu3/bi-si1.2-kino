from django.db import models


class UserModel(models.Model):
    email = models.TextField(unique=False)  # This field type is a guess.
    password = models.TextField(unique=False)  # This field type is a guess.
    name = models.TextField(unique=False)  # This field type is a guess.
    surname = models.TextField(unique=False)  # This field type is a guess.
    user_id = models.BigIntegerField(primary_key=True)
    username = models.TextField(unique=False)  # This field type is a guess.

    class Meta:
        managed = False
        db_table = 'user'
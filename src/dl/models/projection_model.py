from django.db import models


from src.dl.models.hall_model import HallModel
from src.dl.models.movie_model import MovieModel
from src.dl.models.planner_model import PlannerModel
from src.dl.models.projectionist_model import ProjectionistModel  

class ProjectionModel(models.Model):
    ticket_price = models.BigIntegerField(unique=False)
    from_field = models.DateField(db_column='from',
                                  unique=False)  # Field renamed because it was a Python reserved word. This field type is a guess.
    to = models.DateField(unique=False)  # This field type is a guess.
    count_of_viewers = models.BigIntegerField(unique=False)
    projection_id = models.BigIntegerField(primary_key=True)
    movie = models.ForeignKey(MovieModel, models.DO_NOTHING, unique=False)
    hall = models.ForeignKey(HallModel, models.DO_NOTHING, unique=False)
    projectionist = models.ForeignKey(ProjectionistModel, models.DO_NOTHING, unique=False)
    planner = models.ForeignKey(PlannerModel, models.DO_NOTHING, unique=False)

    class Meta:
        managed = False
        db_table = 'projection'

from django.db import models


from src.dl.models.employee_model import  EmployeeModel
from src.dl.models.shift_model import ShiftModel

class HaveShiftModel(models.Model):
    shift = models.ForeignKey(ShiftModel, models.DO_NOTHING, unique=False)
    employee = models.ForeignKey(EmployeeModel, models.DO_NOTHING, primary_key=True)

    class Meta:
        managed = False
        db_table = 'have_shift'

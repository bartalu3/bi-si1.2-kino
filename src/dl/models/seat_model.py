from django.db import models
from src.dl.models.hall_model import HallModel

class SeatModel(models.Model):
    number_of_seat = models.BigIntegerField(unique=False)
    row = models.BigIntegerField(unique=False)
    seat_id = models.BigIntegerField(primary_key=True)
    hall = models.ForeignKey(HallModel, models.DO_NOTHING, unique=False)

    class Meta:
        managed = False
        db_table = 'seat'

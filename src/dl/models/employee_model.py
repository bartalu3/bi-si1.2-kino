from django.db import models
from src.dl.models.user_model import UserModel 


class EmployeeModel(models.Model):
    birhday = models.DateField(unique=False)  # This field type is a guess.
    employee_id = models.BigIntegerField(primary_key=True)
    user = models.ForeignKey(UserModel, models.DO_NOTHING, unique=False)

    class Meta:
        managed = False
        db_table = 'employee'
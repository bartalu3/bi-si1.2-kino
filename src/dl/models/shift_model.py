from django.db import models
from src.dl.models.position_model import PositionModel 

class ShiftModel(models.Model):
    from_field = models.TextField(db_column='from',
                                  unique=False)  # Field renamed because it was a Python reserved word. This field type is a guess.
    to = models.TextField(unique=False)  # This field type is a guess.
    shift_id = models.BigIntegerField(primary_key=True)
    position = models.ForeignKey(PositionModel, models.DO_NOTHING, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'shift'

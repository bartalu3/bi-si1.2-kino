from django.db import models
from src.dl.models.user_model import UserModel



class ClientModel(models.Model):
    client_id = models.BigIntegerField(primary_key=True)
    user = models.ForeignKey(UserModel, models.DO_NOTHING, unique=False)

    class Meta:
        managed = False
        db_table = 'client'
from django.db import models

from src.dl.models.position_model import PositionModel 
from src.dl.models.employee_model import EmployeeModel 
class HavePositionModel(models.Model):
    position = models.ForeignKey(PositionModel, models.DO_NOTHING, unique=False)
    employee = models.ForeignKey(EmployeeModel, models.DO_NOTHING, primary_key=True)

    class Meta:
        managed = False
        db_table = 'have_position'
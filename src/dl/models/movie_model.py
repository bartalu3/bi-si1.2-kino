from django.db import models


## model representation of Movie ...
# Attributes: \n
# price_of_movie price of movie in CZK \n
# title title of movie \n
# movie_id id of movie \n
class MovieModel(models.Model):
    price_of_movie = models.BigIntegerField(unique=False)
    title = models.TextField(unique=False)  # This field type is a guess.
    movie_id = models.IntegerField(primary_key=True)

    class Meta:
        managed = False
        db_table = 'movie'

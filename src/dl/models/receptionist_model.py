from django.db import models


class ReceptionistModel(models.Model):
    receptionist_id = models.IntegerField(primary_key=True)

    class Meta:
        managed = False
        db_table = 'receptionist'

from django.db import models
from src.dl.models.seat_model import SeatModel
from src.dl.models.reservation_model import ReservationModel  

class ReserveSeatModel(models.Model):
    seat = models.ForeignKey(SeatModel, models.DO_NOTHING, unique=False)
    reservation = models.ForeignKey(ReservationModel, models.DO_NOTHING, unique=False)
    reserve_seat_id = models.BigIntegerField(primary_key=True)

    class Meta:
        managed = False
        db_table = 'reserve_seat'

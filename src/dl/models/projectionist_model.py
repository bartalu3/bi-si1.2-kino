from django.db import models
from src.dl.models.position_model import PositionModel 

class ProjectionistModel(models.Model):
    projectionist_id = models.BigIntegerField(primary_key=True)
    position = models.ForeignKey(PositionModel, models.DO_NOTHING, unique=False)

    class Meta:
        managed = False
        db_table = 'projectionist'

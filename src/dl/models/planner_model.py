from django.db import models
from src.dl.models.position_model import PositionModel

class PlannerModel(models.Model):
    planner_id = models.BigIntegerField(primary_key=True)
    position = models.ForeignKey(PositionModel, models.DO_NOTHING, unique=False)

    class Meta:
        managed = False
        db_table = 'planner'

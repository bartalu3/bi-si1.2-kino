from django.db import models


class UsherModel(models.Model):
    usher_id = models.IntegerField(primary_key=True)

    class Meta:
        managed = False
        db_table = 'usher'

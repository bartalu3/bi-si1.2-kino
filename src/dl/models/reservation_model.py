from django.db import models

from src.dl.models.projection_model import ProjectionistModel
from src.dl.models.client_model import ClientModel  

class ReservationModel(models.Model):
    field_date_of_last_change = models.TextField(db_column=' date_of_last_change',
                                                 unique=False)  # Field renamed to remove unsuitable characters. Field renamed because it started with '_'. This field type is a guess.
    date_of_create = models.TextField(unique=False)  # This field type is a guess.
    reservation_id = models.BigIntegerField(primary_key=True)
    projection = models.ForeignKey(ProjectionModel, models.DO_NOTHING, unique=False)
    client = models.ForeignKey(ClientModel, models.DO_NOTHING, unique=False)

    class Meta:
        managed = False
        db_table = 'reservation'

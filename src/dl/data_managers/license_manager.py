from src.dl.data_managers.data_manager import DataManager
from src.dl.models.license_model import LicenseModel
from src.dl.models.movie_model import MovieModel
from src.dl.data_entities.license import License
from src.dl.data_entities.movie import Movie


## managing licenses on database layer
class LicenseManager(DataManager):
    ## get license data entiy by id from models
    # \param self
    # \param id is od of license
    # \return license data entity with id equal to parameter
    def get_by_id(self, id):
        m_license = LicenseModel.objects.all().get(license_id=id)
        m_movie = m_license.movie
        movie = Movie(m_movie.title, m_movie.price_of_movie, m_movie.movie_id)
        license = License(m_license.from_field, m_license.to, m_license.price, movie, m_license.license_id)
        return license

    ## get all license data entities from models
    # \param self
    # \return array of all license data entities from models
    def get_all(self):
        m_licenses = LicenseModel.objects.all()
        licenses = []
        for m_license in m_licenses:
            m_movie = m_license.movie
            movie = Movie(m_movie.title, m_movie.price_of_movie, m_movie.movie_id)
            license = License(m_license.from_field, m_license.to, m_license.price, movie, m_license.license_id)
            licenses.append(license)
        return licenses

    ## add license to models
    # \param self
    # \param license is license that will be stored
    def add(self, license: License):
        m_movie = MovieModel.objects.all().get(movie_id=license.movie)

        m_license = LicenseModel.objects.create(price=license.price, from_field=license.from_field, to=license.to,
                                                movie=m_movie)

    ## remove license from models
    # \param self
    # \param id is id of license that will be removed
    def remove(self, id):
        LicenseModel.objects.filter(license_id=id).remove()

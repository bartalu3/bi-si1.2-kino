from src.dl.data_managers.data_manager import DataManager
from src.dl.models.movie_model import MovieModel
from src.dl.data_entities.movie import Movie

## managing data on database layer
class MovieManager(DataManager):
    ## get movie by id from models
    # \param self
    # \param model_id is id of model which will be returned
    # \return movie data entity from models
    def get_by_id(self, model_id):
        m_movie = MovieModel.objects.all().get(movie_id=id)
        movie = Movie(m_movie.title, m_movie.price_of_movie, m_movie.movie_id)
        return movie

    ## get all movies from models
    # \param self
    # \return array of all movie data entities from models
    def get_all(self):
        m_movies = MovieModel.objects.all()
        movies = []
        for m_movie in m_movies:
            movie = Movie(mv_title=m_movie.title, mv_price=m_movie.price_of_movie, mv_id=m_movie.movie_id)
            movies.append(movie)
        return movies
    ## add movie to models
    # \param self
    # \param movie is movie data entity to be added to models
    def add(self, movie):
        m_movie = MovieModel.objects.create(price_of_movie=movie.price_of_movie, title=movie.title)

    ## removes movie from models
    # \param self
    # \param model_id is id of movie which will be removed from models
    def remove(self, model_id):
        MovieModel.objects.filter(movie_id=id).remove()

    ## gets movie titles with ids from model
    # \param self
    # \return array of all movie titles with ids from models
    def get_titles(self):
        m_movies = MovieModel.objects.all()
        movie_query = []
        for m_movie in m_movies:
            movie = [m_movie.movie_id, m_movie.title]
            movie_query.append(movie)
        return movie_query

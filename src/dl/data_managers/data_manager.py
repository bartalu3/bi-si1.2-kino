import abc


## Abstract base class for data management classes
class DataManager(metaclass=abc.ABCMeta):

    @abc.abstractmethod
    def get_all(self):
        pass

    @abc.abstractmethod
    def get_by_id(self, model_id):
        pass

    @abc.abstractmethod
    def add(self, movie):
        pass

    @abc.abstractmethod
    def remove(self, model_id):
        pass

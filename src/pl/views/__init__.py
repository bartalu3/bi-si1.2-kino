## \mainpage XXXtremeCinema main page
#
# \section intro_sec Introduction
#
# Code documentation to information system for XXXtremeCinema.

__all__ = ["manager", "movies", "licenses"]

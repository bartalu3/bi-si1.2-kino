from django.contrib.auth.decorators import login_required
from django.http import HttpResponse, HttpResponseRedirect
from django.template import loader
from django.utils.decorators import method_decorator
from django.views import View
from src.dl.data_entities.license import License
from src.bl.services.license_services import LicenseServices


## display list of licenses
@method_decorator(login_required, name='dispatch')
class LicenseListView(View):
    template = "licenses/list.html"

    ## get http response to display licenses
    # \param self
    # \param request is http request
    # \return http response
    def get(self, request):
        template = loader.get_template(self.template)
        licenses = LicenseServices().get_all()
        context = {"licenses": licenses, "licenses_count": len(licenses)}
        return HttpResponse(template.render(context, request))

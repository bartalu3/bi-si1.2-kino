from django.contrib.auth.decorators import login_required
from django.http import HttpResponse, HttpResponseRedirect
from django.template import loader
from django.utils.decorators import method_decorator
from django.views import View

from src.pl.forms.license import LicenceForm
from src.dl.data_entities.license import License
from src.bl.services.license_services import LicenseServices


## Display form to create a new license.
@method_decorator(login_required, name='dispatch')
class LicenseNewView(View):
    template = "licenses/new.html"

    ## get http request to create new license
    # \param self
    # \param request is http request
    # \return http response
    def get(self, request):
        template = loader.get_template(self.template)

        form = LicenceForm()
        return HttpResponse(template.render({'form': form}, request))

    ## post request with new license \n
    # if form is valid redirects to '/licenses' page and saves license \n
    # if form is not valid stays on current page
    # \param self
    # \param request is post http request
    # \return http response
    def post(self, request):
        template = loader.get_template(self.template)

        # create a form instance and populate it with data from the request:
        form = LicenceForm(request.POST)
        # check whether it's valid:
        if form.is_valid():
            # passing form to services to save licence to database
            data = form.cleaned_data
            new_license = License(ls_movie=data["movie"], ls_from=data["from_field"], ls_to=data["to"],
                                  ls_price=data["price"])

            LicenseServices().add(new_license)
            # redirect to a new URL:
            return HttpResponseRedirect('/licenses')
        else:
            return HttpResponse(template.render({'form': form}, request))

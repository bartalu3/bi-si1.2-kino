from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
import datetime
from django.template import loader
from django.views import View


class DashboardView(View):
    """
    Display a form to create a new license.
    """

    template = "dashboard.html"

    def get(self, request):
        template = loader.get_template(self.template)
        context = {}
        return HttpResponse(template.render(context, request))

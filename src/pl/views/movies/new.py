from django.contrib.auth.decorators import login_required
from django.http import HttpResponse, HttpResponseRedirect
from django.template import loader
from django.utils.decorators import method_decorator
from django.views import View

from src.pl.forms.movie import MovieForm
from src.dl.data_entities.movie import Movie
from src.bl.services.movie_services import MovieServices

## Display form to create a new movie.
@method_decorator(login_required, name='dispatch')
class MovieNewView(View):
    template = "movies/new.html"

    ## get http request to create new license
    # \param self
    # \param request is http request
    # \return http response
    def get(self, request):
        template = loader.get_template(self.template)
        form = MovieForm()
        return HttpResponse(template.render({'form': form}, request))

    ## post request with new license \n
    # if form is valid redirects to '/movies' page and saves movie \n
    # if form is not valid stays on current page
    # \param self
    # \param request is post http request
    # \return http response
    def post(self, request):
        template = loader.get_template(self.template)
        # create a form instance and populate it with data from the request:
        form = MovieForm(request.POST)
        # check whether it's valid:
        if form.is_valid():
            # passing form to services to save movie to database
            data = form.cleaned_data
            new_movie = Movie(mv_title=data["title"], mv_price=data["price_of_movie"])
            MovieServices().add(new_movie)
            # redirect to a new URL:
            return HttpResponseRedirect('/movies')
        else:
            return HttpResponse(template.render({'form': form}, request))

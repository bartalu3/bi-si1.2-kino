from django.contrib.auth.decorators import login_required
from django.http import HttpResponse, HttpResponseRedirect
from django.template import loader
from django.utils.decorators import method_decorator
from django.views import View
from src.dl.data_entities.movie import Movie
from src.bl.services.movie_services import MovieServices

## display list of movies
@method_decorator(login_required, name='dispatch')
class MovieListView(View):
    template = "movies/list.html"

    ## get hht response to display movies
    # \param self
    # \param request is http request
    # \return http response
    def get(self, request):
        template = loader.get_template(self.template)
        # Get movies data.
        movies = MovieServices().get_all()
        # Pass movies in the template.
        context = {"movies": movies, "movies_count": len(movies)}
        return HttpResponse(template.render(context, request))

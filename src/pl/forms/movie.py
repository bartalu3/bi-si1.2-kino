from django import forms
from src.bl.services.movie_services import MovieServices

## form for adding new movie ...
# attributes: \n
# title (CharField) represents title of movie \n
# price_of_movie (DecimalField) represents price of movie \n
class MovieForm(forms.Form):
    title = forms.CharField(label="Název filmu", required=True, max_length=20, widget=forms.TextInput(
        attrs={"placeholder": "Název filmu", "class": "form-control form-control-user"}))
    price_of_movie = forms.DecimalField(label="Cena vstupného", initial=200.00, min_value=0, max_value=9999999,
                                        required=True,
                                        widget=forms.NumberInput(attrs={"class": "form-control form-control-user"}))

    ## validates title
    # \param self
    # \return corrected title
    def clean_title(self):
        title = self.cleaned_data.get("title")
        return title

    ## valides price of movie
    # \param self
    # \throw Validation error if price is lower than 0
    # \return valid price of movie
    def clean_price_of_movie(self):
        price_of_movie = self.cleaned_data.get("price_of_movie")
        if price_of_movie < 0:
            raise forms.ValidationError("Price of movie cant be less than 0!")
        return price_of_movie

from django import forms
from src.bl.services.movie_services import MovieServices


## form for adding new license ...
# attributes: \n
# price (DecimalField) represents price of license \n
# from_field (DateField) represents date from which is license valid \n
# to (DateField) represents date to which is license valid \n
# movie (ChoiceField) represents movie connected to this license \n
class LicenceForm(forms.Form):
    price = forms.DecimalField(initial=200.00, min_value=0, max_value=99999999999, required=True, label="Cena licence",
                               widget=forms.NumberInput(attrs={"class": "form-control form-control-user"}))
    from_field = forms.DateField(required=True, widget=forms.DateInput(format='%d.%m.%Y', attrs={
        "placeholder": "Od", "class": "form-control form-control-user"}), input_formats=('%d.%m.%Y',))
    to = forms.DateField(required=True,
                         widget=forms.DateInput(format='%d.%m.%Y',
                                                attrs={"placeholder": "Do",
                                                       "class": "form-control form-control-user"}),
                         input_formats=('%d.%m.%Y',))
    movie = forms.ChoiceField()

    ## constructor
    # \param self
    def __init__(self, *args, **kwargs):
        super(LicenceForm, self).__init__(*args, **kwargs)

        # Reload the movies list because of Django cache.
        self.fields['movie'] = forms.ChoiceField(choices=MovieServices().get_titles(), required=True, label="Film",
                                                 widget=forms.Select(attrs={"class": "form-control"}))

    ## validates price of license
    # \param self
    # \throw ValidationError if price is lower than 0
    # \return valid price
    def clean_price(self):
        price = self.cleaned_data.get("price")
        if price < 0:
            raise forms.ValidationError("Price of licence cant be less than 0!")
        return price

    ## validates from_filed of license
    # \param self
    # \return valid date
    def clean_from_field(self):
        from_field = self.cleaned_data.get("from_field")
        return from_field

    ## validates ending date (to) of license
    # \param self
    # \throw ValidationError if start date (from_field) is greater than end date (to)
    # \return valid date
    def clean_to(self):
        from_field = self.cleaned_data.get("from_field")
        to = self.cleaned_data.get("to")
        if from_field > to:
            raise forms.ValidationError("Invalid licence date")
        return to

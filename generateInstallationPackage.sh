#!/bin/bash

mkdir installationPackage
cp -R src installationPackage/src
cp -R cinema installationPackage/cinema
cp -R assets installationPackage/assets
cp manage.py installationPackage/manage.py
cp create.sql installationPackage/create.sql
cp readme.txt installationPackage/readme.txt
cp requirements.txt installationPackage/requirements.txt

tar -zcf installationPackage.tar.gz installationPackage

rm -r installationPackage

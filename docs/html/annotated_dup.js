var annotated_dup =
[
    [ "cleaner_model", "namespacecleaner__model.html", [
      [ "CleanerModel", "classcleaner__model_1_1_cleaner_model.html", "classcleaner__model_1_1_cleaner_model" ]
    ] ],
    [ "client_model", "namespaceclient__model.html", [
      [ "ClientModel", "classclient__model_1_1_client_model.html", "classclient__model_1_1_client_model" ]
    ] ],
    [ "data_manager", "namespacedata__manager.html", [
      [ "DataManager", "classdata__manager_1_1_data_manager.html", "classdata__manager_1_1_data_manager" ]
    ] ],
    [ "django_authModels", "namespacedjango__auth_models.html", [
      [ "AuthGroup", "classdjango__auth_models_1_1_auth_group.html", "classdjango__auth_models_1_1_auth_group" ],
      [ "AuthGroupPermissions", "classdjango__auth_models_1_1_auth_group_permissions.html", "classdjango__auth_models_1_1_auth_group_permissions" ],
      [ "AuthPermission", "classdjango__auth_models_1_1_auth_permission.html", "classdjango__auth_models_1_1_auth_permission" ],
      [ "AuthUser", "classdjango__auth_models_1_1_auth_user.html", "classdjango__auth_models_1_1_auth_user" ],
      [ "AuthUserGroups", "classdjango__auth_models_1_1_auth_user_groups.html", "classdjango__auth_models_1_1_auth_user_groups" ],
      [ "AuthUserUserPermissions", "classdjango__auth_models_1_1_auth_user_user_permissions.html", "classdjango__auth_models_1_1_auth_user_user_permissions" ],
      [ "DjangoAdminLog", "classdjango__auth_models_1_1_django_admin_log.html", "classdjango__auth_models_1_1_django_admin_log" ],
      [ "DjangoContentType", "classdjango__auth_models_1_1_django_content_type.html", "classdjango__auth_models_1_1_django_content_type" ],
      [ "DjangoMigrations", "classdjango__auth_models_1_1_django_migrations.html", "classdjango__auth_models_1_1_django_migrations" ],
      [ "DjangoSession", "classdjango__auth_models_1_1_django_session.html", "classdjango__auth_models_1_1_django_session" ]
    ] ],
    [ "employee_model", "namespaceemployee__model.html", [
      [ "EmployeeModel", "classemployee__model_1_1_employee_model.html", "classemployee__model_1_1_employee_model" ]
    ] ],
    [ "hall_model", "namespacehall__model.html", [
      [ "HallModel", "classhall__model_1_1_hall_model.html", "classhall__model_1_1_hall_model" ]
    ] ],
    [ "have_a_position_model", "namespacehave__a__position__model.html", [
      [ "HavePositionModel", "classhave__a__position__model_1_1_have_position_model.html", "classhave__a__position__model_1_1_have_position_model" ]
    ] ],
    [ "have_a_shift_model", "namespacehave__a__shift__model.html", [
      [ "HaveShiftModel", "classhave__a__shift__model_1_1_have_shift_model.html", "classhave__a__shift__model_1_1_have_shift_model" ]
    ] ],
    [ "license", "namespacelicense.html", [
      [ "LicenceForm", "classlicense_1_1_licence_form.html", "classlicense_1_1_licence_form" ],
      [ "License", "classlicense_1_1_license.html", "classlicense_1_1_license" ]
    ] ],
    [ "license_manager", "namespacelicense__manager.html", [
      [ "LicenseManager", "classlicense__manager_1_1_license_manager.html", "classlicense__manager_1_1_license_manager" ]
    ] ],
    [ "license_model", "namespacelicense__model.html", [
      [ "LicenseModel", "classlicense__model_1_1_license_model.html", "classlicense__model_1_1_license_model" ]
    ] ],
    [ "license_services", "namespacelicense__services.html", [
      [ "LicenseServices", "classlicense__services_1_1_license_services.html", "classlicense__services_1_1_license_services" ]
    ] ],
    [ "movie", "namespacemovie.html", [
      [ "Movie", "classmovie_1_1_movie.html", "classmovie_1_1_movie" ],
      [ "MovieForm", "classmovie_1_1_movie_form.html", "classmovie_1_1_movie_form" ]
    ] ],
    [ "movie_manager", "namespacemovie__manager.html", [
      [ "MovieManager", "classmovie__manager_1_1_movie_manager.html", "classmovie__manager_1_1_movie_manager" ]
    ] ],
    [ "movie_model", "namespacemovie__model.html", [
      [ "MovieModel", "classmovie__model_1_1_movie_model.html", "classmovie__model_1_1_movie_model" ]
    ] ],
    [ "movie_services", "namespacemovie__services.html", [
      [ "MovieServices", "classmovie__services_1_1_movie_services.html", "classmovie__services_1_1_movie_services" ]
    ] ],
    [ "planner", "namespaceplanner.html", [
      [ "Planner", "classplanner_1_1_planner.html", "classplanner_1_1_planner" ]
    ] ],
    [ "planner_model", "namespaceplanner__model.html", [
      [ "PlannerModel", "classplanner__model_1_1_planner_model.html", "classplanner__model_1_1_planner_model" ]
    ] ],
    [ "position_model", "namespaceposition__model.html", [
      [ "PositionModel", "classposition__model_1_1_position_model.html", "classposition__model_1_1_position_model" ]
    ] ],
    [ "projection_model", "namespaceprojection__model.html", [
      [ "ProjectionModel", "classprojection__model_1_1_projection_model.html", "classprojection__model_1_1_projection_model" ]
    ] ],
    [ "projectionist_model", "namespaceprojectionist__model.html", [
      [ "ProjectionistModel", "classprojectionist__model_1_1_projectionist_model.html", "classprojectionist__model_1_1_projectionist_model" ]
    ] ],
    [ "receptionist_model", "namespacereceptionist__model.html", [
      [ "ReceptionistModel", "classreceptionist__model_1_1_receptionist_model.html", "classreceptionist__model_1_1_receptionist_model" ]
    ] ],
    [ "reservation_model", "namespacereservation__model.html", [
      [ "ReservationModel", "classreservation__model_1_1_reservation_model.html", "classreservation__model_1_1_reservation_model" ]
    ] ],
    [ "reserve_seat_model", "namespacereserve__seat__model.html", [
      [ "ReserveSeatModel", "classreserve__seat__model_1_1_reserve_seat_model.html", "classreserve__seat__model_1_1_reserve_seat_model" ]
    ] ],
    [ "seat_model", "namespaceseat__model.html", [
      [ "SeatModel", "classseat__model_1_1_seat_model.html", "classseat__model_1_1_seat_model" ]
    ] ],
    [ "shift_model", "namespaceshift__model.html", [
      [ "ShiftModel", "classshift__model_1_1_shift_model.html", "classshift__model_1_1_shift_model" ]
    ] ],
    [ "user_model", "namespaceuser__model.html", [
      [ "UserModel", "classuser__model_1_1_user_model.html", "classuser__model_1_1_user_model" ]
    ] ],
    [ "usher_model", "namespaceusher__model.html", [
      [ "UsherModel", "classusher__model_1_1_usher_model.html", "classusher__model_1_1_usher_model" ]
    ] ],
    [ "views", "namespaceviews.html", [
      [ "licenses", "namespaceviews_1_1licenses.html", [
        [ "list", "namespaceviews_1_1licenses_1_1list.html", [
          [ "LicenseListView", "classviews_1_1licenses_1_1list_1_1_license_list_view.html", "classviews_1_1licenses_1_1list_1_1_license_list_view" ]
        ] ],
        [ "new", "namespaceviews_1_1licenses_1_1new.html", [
          [ "LicenseNewView", "classviews_1_1licenses_1_1new_1_1_license_new_view.html", "classviews_1_1licenses_1_1new_1_1_license_new_view" ]
        ] ]
      ] ],
      [ "manager", "namespaceviews_1_1manager.html", [
        [ "dashboard", "namespaceviews_1_1manager_1_1dashboard.html", [
          [ "DashboardView", "classviews_1_1manager_1_1dashboard_1_1_dashboard_view.html", "classviews_1_1manager_1_1dashboard_1_1_dashboard_view" ]
        ] ]
      ] ],
      [ "movies", "namespaceviews_1_1movies.html", [
        [ "list", "namespaceviews_1_1movies_1_1list.html", [
          [ "MovieListView", "classviews_1_1movies_1_1list_1_1_movie_list_view.html", "classviews_1_1movies_1_1list_1_1_movie_list_view" ]
        ] ],
        [ "new", "namespaceviews_1_1movies_1_1new.html", [
          [ "MovieNewView", "classviews_1_1movies_1_1new_1_1_movie_new_view.html", "classviews_1_1movies_1_1new_1_1_movie_new_view" ]
        ] ]
      ] ]
    ] ]
];
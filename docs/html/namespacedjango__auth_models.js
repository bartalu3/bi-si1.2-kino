var namespacedjango__auth_models =
[
    [ "AuthGroup", "classdjango__auth_models_1_1_auth_group.html", "classdjango__auth_models_1_1_auth_group" ],
    [ "AuthGroupPermissions", "classdjango__auth_models_1_1_auth_group_permissions.html", "classdjango__auth_models_1_1_auth_group_permissions" ],
    [ "AuthPermission", "classdjango__auth_models_1_1_auth_permission.html", "classdjango__auth_models_1_1_auth_permission" ],
    [ "AuthUser", "classdjango__auth_models_1_1_auth_user.html", "classdjango__auth_models_1_1_auth_user" ],
    [ "AuthUserGroups", "classdjango__auth_models_1_1_auth_user_groups.html", "classdjango__auth_models_1_1_auth_user_groups" ],
    [ "AuthUserUserPermissions", "classdjango__auth_models_1_1_auth_user_user_permissions.html", "classdjango__auth_models_1_1_auth_user_user_permissions" ],
    [ "DjangoAdminLog", "classdjango__auth_models_1_1_django_admin_log.html", "classdjango__auth_models_1_1_django_admin_log" ],
    [ "DjangoContentType", "classdjango__auth_models_1_1_django_content_type.html", "classdjango__auth_models_1_1_django_content_type" ],
    [ "DjangoMigrations", "classdjango__auth_models_1_1_django_migrations.html", "classdjango__auth_models_1_1_django_migrations" ],
    [ "DjangoSession", "classdjango__auth_models_1_1_django_session.html", "classdjango__auth_models_1_1_django_session" ]
];
var classprojection__model_1_1_projection_model =
[
    [ "Meta", "classprojection__model_1_1_projection_model_1_1_meta.html", "classprojection__model_1_1_projection_model_1_1_meta" ],
    [ "count_of_viewers", "classprojection__model_1_1_projection_model.html#a642e43f04494c4916e89c7f3f29f8039", null ],
    [ "from_field", "classprojection__model_1_1_projection_model.html#a72364530245957565e29d0bba0b24d48", null ],
    [ "hall", "classprojection__model_1_1_projection_model.html#aaa6043a7254db0c11743225f1a0e6845", null ],
    [ "movie", "classprojection__model_1_1_projection_model.html#ae484bbb4e2af6e2c896075fdb5c69e3e", null ],
    [ "planner", "classprojection__model_1_1_projection_model.html#ac0a6c1ac0e2a634305168519b4a4fa34", null ],
    [ "projection_id", "classprojection__model_1_1_projection_model.html#ab3b5ab45937bc6b3e272c33efe0be546", null ],
    [ "projectionist", "classprojection__model_1_1_projection_model.html#a3391dcce4a2d5665a865c2fa3fa9c766", null ],
    [ "ticket_price", "classprojection__model_1_1_projection_model.html#ad7271b82f8580df97c481e6677320053", null ],
    [ "to", "classprojection__model_1_1_projection_model.html#af71dbe52628a3f83a77ab494817525c6", null ]
];
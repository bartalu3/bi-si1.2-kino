var classdjango__auth_models_1_1_django_admin_log =
[
    [ "Meta", "classdjango__auth_models_1_1_django_admin_log_1_1_meta.html", "classdjango__auth_models_1_1_django_admin_log_1_1_meta" ],
    [ "action_flag", "classdjango__auth_models_1_1_django_admin_log.html#a0d6bf50e430e0c27587dfbc61dd9c10b", null ],
    [ "action_time", "classdjango__auth_models_1_1_django_admin_log.html#a83394fc89267c1757c1a5e293916d6f9", null ],
    [ "change_message", "classdjango__auth_models_1_1_django_admin_log.html#ae227b4c6efdc1db323ce50584584ece4", null ],
    [ "content_type", "classdjango__auth_models_1_1_django_admin_log.html#a221623422ea72de630e7e446a84e9f1f", null ],
    [ "id", "classdjango__auth_models_1_1_django_admin_log.html#acf2488b95c97e0378c9bf49de3b50f28", null ],
    [ "object_id", "classdjango__auth_models_1_1_django_admin_log.html#a1493ea6a653e4034d9a94a81ab6f2681", null ],
    [ "object_repr", "classdjango__auth_models_1_1_django_admin_log.html#a107d10676c127c27454114aa76415cae", null ],
    [ "user", "classdjango__auth_models_1_1_django_admin_log.html#a5cc32e366c87c4cb49e4309b75f57d64", null ]
];
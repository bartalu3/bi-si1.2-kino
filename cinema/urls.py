"""cinema URL Configuration

The `urlpatterns` list.html routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static
from src.pl.views import *
from src.pl.views.manager import *
from src.pl.views.movies import *
from src.pl.views.licenses import *
from django.contrib.auth import views as auth_views

urlpatterns = [
                  # Main page and adminer
                  path("", manager.dashboard.DashboardView.as_view(), name="home"),
                  path('home/', manager.dashboard.DashboardView.as_view()),
                  path('admin/', admin.site.urls),

                  # Movies
                  path("movies/", movies.list.MovieListView.as_view(), name="movies/list"),
                  path("movies/list", movies.list.MovieListView.as_view()),
                  path("movies/new", movies.new.MovieNewView.as_view(), name="movies/new"),

                  # Users
                  # path("login/", users.login, name="login"),
                  path('login/', auth_views.LoginView.as_view(template_name='registration/login.html'), name="login"),
                  path('logout/', auth_views.LogoutView.as_view(), name="logout"),
                  path('accounts/', include('django.contrib.auth.urls')),

                  # Licenses
                  path("licenses/", licenses.list.LicenseListView.as_view(), name="licenses/list"),
                  path("licenses/list", licenses.list.LicenseListView.as_view()),
                  path("licenses/new", licenses.new.LicenseNewView.as_view(), name="licenses/new"),

              ] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

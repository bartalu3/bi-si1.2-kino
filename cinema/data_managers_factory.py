from src.dl.data_managers.license_manager import LicenseManager
from src.dl.data_managers.movie_manager import MovieManager


class DataManagerFactory:
    """
    Data managers dependencies factory.
    """

    Movies = MovieManager()
    Licenses = LicenseManager()
